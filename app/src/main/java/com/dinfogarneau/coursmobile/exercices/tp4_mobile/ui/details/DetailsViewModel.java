package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.details;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class DetailsViewModel extends AndroidViewModel {

    public DetailsViewModel(@NonNull Application application) {
        super(application);
    }
}
