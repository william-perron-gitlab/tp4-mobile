package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Produit;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class DetailsFragment extends Fragment {

    private static final String TAG = "TAG";
    private DetailsViewModel detailsViewModel;
    private RecyclerView rvDetails;
    private ArrayList<Produit> produitsList;
    private DetailsAdapter detailsAdapter;
    private Magasin magasin;
    private FloatingActionButton btAdd;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference Cr = db.collection("produits");

    ListenerRegistration registration;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        detailsViewModel = new ViewModelProvider(requireActivity()).get(DetailsViewModel.class);
        magasin = DetailsFragmentArgs.fromBundle(getArguments()).getMagasin();
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvDetails = view.findViewById(R.id.rv_details);
        produitsList = new ArrayList<>();
        detailsAdapter = new DetailsAdapter(produitsList);
        rvDetails.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvDetails.setAdapter(detailsAdapter);

        btAdd = view.findViewById(R.id.bt_add_produit);

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAjoutFragment newFragment = new DialogAjoutFragment(magasin);
                newFragment.show(getActivity().getSupportFragmentManager(), "Ajouter produit");
            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int itemPosition = viewHolder.getAdapterPosition();
                detailsAdapter.notifyItemRemoved(itemPosition);
                Cr.document(produitsList.get(itemPosition).getId()).delete();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvDetails);
    }

    @Override
    public void onStart() {
        super.onStart();
        registration = Cr
                .whereEqualTo("nomMagasin", magasin.getNom())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        produitsList.clear();
                        for (QueryDocumentSnapshot document : value) {
                            Produit produit = document.toObject(Produit.class);
                            produit.setId(document.getId());
                            produitsList.add(produit);
                        }
                        detailsAdapter.notifyDataSetChanged();
                    }
                });
    }
}
