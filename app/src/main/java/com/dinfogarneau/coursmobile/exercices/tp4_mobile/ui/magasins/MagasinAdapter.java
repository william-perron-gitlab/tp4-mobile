package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.magasins;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;

import java.util.ArrayList;
import java.util.List;

public class MagasinAdapter extends RecyclerView.Adapter<MagasinAdapter.MagasinViewHolder> {

    private List<Magasin> magasinList;

    public MagasinAdapter(ArrayList<Magasin> magasins) {
        this.magasinList = magasins;
    }

    private onItemClickListener mListener;

    public void setMagasinsList(List<Magasin> magasins) { magasinList = magasins; }

    public List<Magasin> getMagasinList() { return magasinList; }

    public interface onItemClickListener {
        void naviguerDetails(Magasin magasin);
    }

    public void setOnClickListener(onItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public MagasinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.magasin_item, parent, false);
        return new MagasinViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MagasinViewHolder holder, int position) {
        if (magasinList != null) {
            Magasin current = magasinList.get(position);
            holder.tvNom.setText(current.getNom());
        }
    }

    @Override
    public int getItemCount() {
        if (magasinList != null)
            return magasinList.size();
        else return 0;
    }

    public class MagasinViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNom;

        public MagasinViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNom = itemView.findViewById(R.id.tv_nom_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        // Bonne pratique pour s'assurer que l'élément clické est toujours présent
                        if (position != RecyclerView.NO_POSITION) {
                            Magasin magasin = magasinList.get(position);
                            mListener.naviguerDetails(magasin);
                        }
                    }
                }
            });
        }
    }
}
