package com.dinfogarneau.coursmobile.exercices.tp4_mobile.services;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;

public class HackedService  extends androidx.core.app.JobIntentService {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, HackedService.class, 123, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        for (int i = 0; i < 10; i++) {
            double minLat = 46.7;
            double maxLat = 46.8;
            double minLon = -71.30;
            double maxLon = -71.20;
            Random lat = new Random();
            Random lon = new Random();
            double latitude = minLat + (maxLat - minLat) * lat.nextDouble();
            double longitude = minLon + (maxLon - minLon) * lon.nextDouble();
            Magasin magasin = new Magasin("HACKED", longitude, latitude);
            db.collection("magasins")
                    .add(magasin);
            if (isStopped()) return;
            SystemClock.sleep(1000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onStopCurrentWork() {
        return super.onStopCurrentWork();
    }
}
