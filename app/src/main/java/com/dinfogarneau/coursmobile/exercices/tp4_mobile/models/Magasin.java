package com.dinfogarneau.coursmobile.exercices.tp4_mobile.models;

import java.io.Serializable;

public class Magasin implements Serializable {

    private String id;
    private String nom;
    private double longitude;
    private double latitude;

    public Magasin() {}

    public Magasin(String nom, double longitude, double latitude) {

        this.nom = nom;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
