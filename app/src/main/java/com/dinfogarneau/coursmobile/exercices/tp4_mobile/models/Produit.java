package com.dinfogarneau.coursmobile.exercices.tp4_mobile.models;

import java.io.Serializable;

public class Produit implements Serializable {

    private String id;
    private String nom;
    private double prix;
    private String pseudo;
    private String nomMagasin;

    public Produit() {
    }

    public Produit(String nom, double prix, String pseudo, String nomMagasin) {
        this.nom = nom;
        this.prix = prix;
        this.pseudo = pseudo;
        this.nomMagasin = nomMagasin;
    }

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getNomMagasin() {
        return nomMagasin;
    }

    public void setNomMagasin(String nomMagasin) {
        this.nomMagasin = nomMagasin;
    }
}
