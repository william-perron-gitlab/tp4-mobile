package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.magasins.MagasinAdapter;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.magasins.MagasinViewModel;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.magasins.MagasinsFragmentDirections;

import java.util.ArrayList;

public class ConfigFragment extends Fragment {

    private static final String SHARED_PREFERENCES = "MyPref";
    private static final String PSEUDO_KEY = "pseudo";
    private static final String DEFAULT_PSEUDO = "Garneau";

    private EditText etPseudo;
    private Button btEnregistrer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_config, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etPseudo = view.findViewById(R.id.et_pseudo);
        btEnregistrer = view.findViewById(R.id.bt_enregistrer);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String pseudo = sharedPreferences.getString(PSEUDO_KEY, DEFAULT_PSEUDO);

        etPseudo.setText(pseudo);

        btEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(PSEUDO_KEY, etPseudo.getText().toString());
                editor.apply();
                Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(
                        R.id.action_nav_config_to_nav_liste
                );
            }
        });
    }
}
