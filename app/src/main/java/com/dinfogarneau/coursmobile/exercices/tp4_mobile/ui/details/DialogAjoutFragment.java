package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Produit;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;

public class DialogAjoutFragment extends DialogFragment {

    private static final String AJOUTER_TITLE = "Ajouter un produit";
    private static final String AJOUTER = "Ajouter";
    private static final String ANNULER = "Annuler";
    private static final String ERREUR = "Le nom ne doit pas être vide";

    private static final String SHARED_PREFERENCES = "MyPref";
    private static final String PSEUDO_KEY = "pseudo";
    private static final String DEFAULT_PSEUDO = "Garneau";

    private String nom;
    private double prix;
    private Magasin magasin;

    public interface AjoutProduitDialogListener {
        void onFinishEditDialog(Produit produit);
    }

    public DialogAjoutFragment(Magasin magasin) {
        this.magasin = magasin;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String formButton = AJOUTER;

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.produit_form, null);
        final Context context = getContext();

        builder.setView(v)
                .setPositiveButton(formButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText etNomProduit = v.findViewById(R.id.et_nom);
                        EditText etPrix = v.findViewById(R.id.et_prix);

                        String nomTrim = etNomProduit.getText().toString().trim();
                        double prixProduit = Double.parseDouble(etPrix.getText().toString());

                        if (nomTrim.isEmpty()) {
                            Toast.makeText(context, ERREUR, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
                        String pseudo = sharedPreferences.getString(PSEUDO_KEY, DEFAULT_PSEUDO);

                        Produit produit = new Produit(nomTrim, prixProduit, pseudo, magasin.getNom());

                        AjoutProduitDialogListener listener = (AjoutProduitDialogListener) getActivity();
                        listener.onFinishEditDialog(produit);
                    }

                })
                .setNegativeButton(ANNULER, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        // Create the AlertDialog object and return it

        builder.setTitle(AJOUTER_TITLE);
        return builder.create();
    }
}
