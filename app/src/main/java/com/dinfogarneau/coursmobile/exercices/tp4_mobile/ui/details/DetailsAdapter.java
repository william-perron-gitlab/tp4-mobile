package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Produit;

import java.util.ArrayList;
import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.DetailsViewHolder> {

    private List<Produit> produitList;

    public DetailsAdapter(ArrayList<Produit> produits) {
        this.produitList = produits;
    }

    public void setProduitList(List<Produit> produits) { produitList = produits; }

    public List<Produit> getProduitList() { return produitList; }

    @NonNull
    @Override
    public DetailsAdapter.DetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_item, parent, false);
        return new DetailsAdapter.DetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsAdapter.DetailsViewHolder holder, int position) {
        if (produitList != null) {
            Produit current = produitList.get(position);
            holder.tvNom.setText(current.getNom());
            holder.tvPrix.setText(Double.toString(current.getPrix()));
            holder.tvPseudo.setText(current.getPseudo());
        }
    }

    @Override
    public int getItemCount() {
        if (produitList != null)
            return produitList.size();
        else return 0;
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNom;
        public TextView tvPrix;
        public TextView tvPseudo;

        public DetailsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNom = itemView.findViewById(R.id.tv_nom_item);
            tvPrix = itemView.findViewById(R.id.tv_prix_detail);
            tvPseudo = itemView.findViewById(R.id.tv_pseudo_detail);
        }
    }
}
