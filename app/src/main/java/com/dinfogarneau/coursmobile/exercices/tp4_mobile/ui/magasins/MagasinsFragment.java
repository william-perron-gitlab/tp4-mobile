package com.dinfogarneau.coursmobile.exercices.tp4_mobile.ui.magasins;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dinfogarneau.coursmobile.exercices.tp4_mobile.R;

import com.dinfogarneau.coursmobile.exercices.tp4_mobile.models.Magasin;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagasinsFragment extends Fragment {

    private static final String TAG = "TAG";
    private MagasinViewModel magasinViewModel;
    private RecyclerView rvMagasins;
    private ArrayList<Magasin> magasinsList;
    private MagasinAdapter magasinAdapter;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference Cr = db.collection("magasins");
    DocumentReference magasinReg = Cr.document("magasin1");
    ListenerRegistration registration;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        magasinViewModel = new ViewModelProvider(requireActivity()).get(MagasinViewModel.class);

        return inflater.inflate(R.layout.fragment_magasins, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvMagasins = view.findViewById(R.id.rv_proprietes);
        magasinsList = new ArrayList<>();
        magasinAdapter = new MagasinAdapter(magasinsList);
        rvMagasins.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvMagasins.setAdapter(magasinAdapter);

        magasinAdapter.setOnClickListener(new MagasinAdapter.onItemClickListener() {
            @Override
            public void naviguerDetails(Magasin magasin) {
                NavDirections action = MagasinsFragmentDirections.actionNavListeToNavCarte();
                Navigation.findNavController(view).navigate(action);
            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int itemPosition = viewHolder.getAdapterPosition();
                magasinAdapter.notifyItemRemoved(itemPosition);
                Cr.document(magasinsList.get(itemPosition).getId()).delete();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvMagasins);
    }

    @Override
    public void onStart() {
        super.onStart();
        registration = Cr
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        magasinsList.clear();
                        for (QueryDocumentSnapshot document : value) {
                            Magasin magasin = document.toObject(Magasin.class);
                            magasin.setId(document.getId());
                            magasinsList.add(magasin);
                        }
                        magasinAdapter.notifyDataSetChanged();
                    }
                });
    }
}
